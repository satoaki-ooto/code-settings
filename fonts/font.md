# install fonts list

* [Juisee](https://github.com/yuru7/juisee)
* [Nerd Fonts](https://github.com/ryanoasis/nerd-fonts)
    * [Caskaydia Cove Nerd Font](https://github.com/ryanoasis/nerd-fonts/releases/download/v3.0.2/CascadiaCode.zip)
    * [Blex Mono](https://github.com/ryanoasis/nerd-fonts/releases/download/v3.0.2/IBMPlexMono.zip)
    * [JetBrains Mono](https://github.com/ryanoasis/nerd-fonts/releases/download/v3.0.2/JetBrainsMono.zip)