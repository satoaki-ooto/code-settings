#!/bin/bash

# set filename
filename="hyper.js"

# download configuration
curl -fsSL https://gitlab.com/satoaki-ooto/code-settings/-/raw/main/hyper/hyper.js --output $HOME/$filename

# copy configuiration file
cp $HOME/$filename $HOME/.hyper.js
rm $HOME/$filename

# display message
echo "launch hyper.js"