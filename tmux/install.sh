#!/bin/bash

# set filename
filename="tmux.conf"

# install package maneger
git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
curl -fsSL https://gitlab.com/satoaki-ooto/code-settings/-/raw/main/tmux/tmux.conf --output $HOME/$filename

# copy configuiration file
cp $HOME/$filename $HOME/.tmux.conf
rm $HOME/$filename

# display message
echo "launch tmux process, enter Prefix-Key + I"